#include"ProcessBase.h"


class ProQueue{
public:
	ProQueue();
	void setTimeLenth(int n);
	virtual ~ProQueue();

	PCB*dequeue();
	void inqueue(PCB*p);
	bool empty(){
		return ! e_num;
	}
	int timeLength(){
		return time_length;
	}
	PCB*head(){
		return _queue;
	}
private:
	PCB * _queue;
	PCB *_rear;
	int time_length;
	int e_num;
};

class MuFedBk_Processer :public BaseProcesser {

	virtual void insert_back();
	virtual void first_in();
	virtual void do_creat(int needtime);
	virtual bool do_job();
	virtual void print_state();

	ProQueue * proqueues;	
	int now_que_level;
	
public:
	MuFedBk_Processer(int n);
};