#pragma once
//刚刚看了点设计模式，在此例中这里用到了很多 template method 王哥是天才吗？
#include<string>


enum State{
	RUN,RDY,FNS
};
enum ClassType{
	RR,Prio,MtF
};
std::string theState(State s);

struct PCB {
	PCB():name(0),prio(0),cputime(0),needtime(50),next(nullptr),pre(nullptr){}
	int name;
	int prio;
	int cputime;
	State state;
	int needtime;
	PCB * next;
	PCB * pre;
};

class BaseProcesser {
protected:
	int p_count;    //进程容量
	PCB* running;   //运行进程

	int p_num;		//当前进程数
	int fini_num;	//已完成的进程数
	PCB* head;		//所有PCB
	PCB* empty;     //空闲PCB链头
	PCB* readyH;    //就绪头
	PCB* readyT;    //就绪尾
	PCB* finish;
	bool to_exit;

	ClassType type;
public:
	BaseProcesser(int p_num);
	virtual ~BaseProcesser();
	
	
	
	void do_finish();
	void do_exit();
	bool all_finish();
	void print();
	// template method
	void creat(int time_or_prio);//对于轮转法是名，对于优先级法是优先级
	void schadule();
	void job();

     
	virtual void print_state();
	virtual bool do_job()=0; //工作做完返回true
	virtual void insert_back()=0; //插回
	virtual void first_in()=0;
	virtual void do_creat(int name_or_prio)=0;//对于轮转法是名，对于优先级法是优先级
};

