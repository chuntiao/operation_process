#include "header/RR_Processer.h"
#include "header/Prio_Processer.h"
#include "header/MtFedBk_Processer.h"

#include<iostream>

int main(){
    
    int op=0;
    BaseProcesser* pro;
    std::cout<<"input your option:\n 1.RR 2.Priority 3.Num-Classes\n>";
    std::cin>>op;
    std::cout<<"input the volume of processes \n>";
    int vol;
    std::cin>>vol;

    int num;
    std::cout<<"input the num of processes \n>";
    std::cin>>num;
    if(op==1){
        
        pro=new RR_Processer(vol);
    }
    else if(op==2)
    {
        pro=new Prio_Processer(vol);
    }
    else if (op==3)
    {
        pro=new MuFedBk_Processer(vol);
    }
    
    std::cout<<"input prio or time_needed of each process \n>";
    int o;
    while(num--){
        std::cin>>o;
        pro->creat(o);
    }
    
    
    while(!pro->all_finish()){
        pro->schadule();
    }
//最终一次打印
    pro->print();
    delete pro;
    std::cout<<"press any key to exit";
    std::cin.get();
    std::cin.get();
    return 0;
}