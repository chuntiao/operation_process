gg=g++ -std=c++11 
objects=*.o  
a.exe:$(objects)
	$(gg) -o a.exe  $(objects) 
*.o:*.cpp  
	$(gg)  -c *.cpp 
.PHONY : clean 

clean:
	del  $(objects)