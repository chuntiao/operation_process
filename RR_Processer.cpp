#include "header/RR_Processer.h"

RR_Processer::RR_Processer(int n):BaseProcesser(n){
    type=RR;
}
void RR_Processer::insert_back()
{
    readyT->next=running;
    running->pre=readyT;
    readyT= readyT->next;
    readyT->next=nullptr;
    if(readyH==nullptr)
        readyH=readyT;
    if(!readyT)
        do_exit();
}

void RR_Processer::first_in()
{
    if(readyH){
        running = readyH;
        running->state=State::RUN;
        readyH=readyH->next;
        if(readyH)
            readyH->pre=nullptr;
    }
}
void RR_Processer::do_creat(int n){
    readyT->next=empty;
    empty->pre=readyT;
    empty = empty->next;
    readyT=readyT->next;
    readyT->next=nullptr;
    readyT->cputime=0;
    readyT->needtime=n;
    readyT->state=State::RDY;
    p_num++;
}
bool RR_Processer::do_job(){
    running->cputime+=2;
    running->needtime-=2;
    return running->needtime<=0;
}
